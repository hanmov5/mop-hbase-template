package com.gaoxin.mop.dao;


import com.gaoxin.mop.bean.ColumnInfo;

import java.util.List;

/**
 * Author: Mr.tan
 * Date:  2017/08/18
 */
public interface HBaseDao {

    /**
     * 获取单个rowKey下的column封装为对应PO
     */
    <T> T get(String tableName, String rowKey, List<ColumnInfo> columns, List<ColumnInfo> filters, Class<? extends T> clazz);

    <T> T get(String tableName, String rowKey, Class<? extends T> clazz);

    <T> T get(String tableName, String rowKey, List<ColumnInfo> columns, Class<? extends T> clazz);

    /**
     * 获取单个column的value
     */
    String getSingleColumnValue(String tableName, String rowKey, String column);

    <T> T getSingleColumnValue(String tableName, String rowKey, String column, Class<? extends T> clazz);

    /**
     * 获取所有rowKey
     */
    List<String> getRowKeys(String tableName);

    List<String> getRowKeys(String tableName, String startRow, String endRow);

    List<String> getRowKeys(String tableName, String startRow, String endRow, Integer pageSize, String separate, Integer index);

    List<String> getRowKeys(String tableName, String startRow, String endRow, Integer pageSize, String separate);

    List<String> getRowKeys(String tableName, String startRow, String endRow, Integer pageSize);

    /**
     * 获取所有rowKey 前缀过滤
     */
    List<String> getRowKeysByPrefix(String tableName, String prefix);

    List<String> getRowKeysByPrefix(String tableName, String startRow, String endRow, String prefix);

    /**
     * 获取单个rowKey下的column
     */
    List<ColumnInfo> getColumns(String tableName, String rowKey, String columnFamily, List<ColumnInfo> columns, List<ColumnInfo> filters);

    List<ColumnInfo> getColumns(String tableName, String rowKey, List<ColumnInfo> columns, List<ColumnInfo> filters);

    List<ColumnInfo> getColumns(String tableName, String rowKey, List<String> columns);

    List<ColumnInfo> getColumns(String tableName, String rowKey, String columnFamily);

    List<ColumnInfo> getColumns(String tableName, String rowKey);


    /**
     * 获取多个rowKey对应的column封装为PO list
     */
    <T> List<T> getList(String tableName, List<String> rowKeys, Class<? extends T> clazz);

    <T> List<T> getList(String tableName, List<String> rowKeys, List<ColumnInfo> columns, List<ColumnInfo> filters, Class<? extends T> clazz);

    <T> List<T> getList(String tableName, Class<? extends T> clazz);

    <T> List<T> getList(String tableName, List<ColumnInfo> columns, List<ColumnInfo> filters, Class<? extends T> clazz);

    <T> List<T> getList(String tableName, List<ColumnInfo> columns, List<ColumnInfo> filters, String start, String end, Class<? extends T> clazz);

    /**
     * 获取多个rowKey对应的column封装为PO list start->end 分页
     */
    <T> List<T> getPageList(String tableName, String startRow, String endRow, Integer pageSize, Class<? extends T> clazz);

    /**
     * 获取多个rowKey对应的column封装为PO list start->end 分页
     */
    <T> List<T> getPageList(String tableName, String startRow, String endRow, Integer pageSize, List<ColumnInfo> columns, List<ColumnInfo> filters, Class<? extends T> clazz);

    /**
     * 获取rowKey对应的column 偏移量分页
     */
    List<ColumnInfo> getColumnsByPage(String tableName, String rowKey, Integer pageNo, Integer pageSize);

    /**
     * 获取rowKey对应的column
     */
    List<ColumnInfo> getColumnsByPage(String tableName, String rowKey, Integer pageSize, String startColumn, String endColumn);

    /**
     * 获取rowKey对应的column
     */
    List<ColumnInfo> getColumnsByPage(String tableName, String rowKey, Integer pageSize, String startColumn, String endColumn, List<ColumnInfo> filters);

    List<ColumnInfo> getColumnsByPage(String tableName, String rowKey, Integer pageNo, Integer pageSize, List<ColumnInfo> columns, List<ColumnInfo> filters);

    /**
     * 获取多个rowKey对应的单个column封装为对象 适用于value为JSON
     */
    <T> T getColumnObj(String tableName, String rowKey, String column, Class<? extends T> clazz);

    /**
     * 获取多个rowKey对应的多个column封装为对象 适用于value为JSON
     */
    <T> List<T> getColumnObjList(String tableName, String rowKey, List<String> columns, Class<? extends T> clazz);

    /**
     * 获取多个rowKey对应的多个column封装为对象 适用于value为JSON，可分页
     */
    <T> List<T> getPageColumnObjList(String tableName, String rowKey, Integer pageNo, Integer pageSize, Class<? extends T> clazz);


    /**
     * put多个PO
     */
    <T> boolean put(String tableName, List<T> objects);

    /**
     * put单个PO
     */
    <T> boolean put(String tableName, T object);


    /**
     * put单个column和value
     */
    boolean put(String tableName, String rowKey, String column, String value);

    boolean put(String tableName, String rowKey, ColumnInfo columnInfo);

    boolean put(String tableName, String rowKey, List<ColumnInfo> list);


    /**
     * delete多个rowKey
     */
    boolean delete(String tableName, List<String> rowKeys);

    boolean delete(String tableName, String rowKey);

    /**
     * delete对应的column
     */
    boolean delete(String tableName, String rowKey, List<ColumnInfo> list);

    boolean delete(String tableName, String rowKey, ColumnInfo columnInfo);

    boolean delete(String tableName, String rowKey, String column);

    /**
     * 原子计数器 num可为负
     */
    long addCounter(String tableName, String rowKey, String column, long num);

    /**
     * 判断column是否存在
     */
    boolean exists(String tableName, String rowKey, String column);

    /**
     * put原子操作 cas
     */
    boolean checkAndPut(String tableName, String rowKey, String column, String oldValue, String newValue);

    /**
     * delete原子操作 cas
     */
    boolean checkAndDelete(String tableName, String rowKey, String column, String oldValue);
}
