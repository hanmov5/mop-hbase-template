package com.gaoxin.mop;

/**
 * 存放于book的收藏历史和阅读历史的columnvalue的json对象
 *
 * Author: Mr.tan
 * Date:  2017/09/19
 */
public class BookValue {

    private String bookid;

    private String bookdetail;


    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public String getBookdetail() {
        return bookdetail;
    }

    public void setBookdetail(String bookdetail) {
        this.bookdetail = bookdetail;
    }
}
