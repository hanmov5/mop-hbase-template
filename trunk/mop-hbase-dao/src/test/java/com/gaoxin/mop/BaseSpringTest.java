package com.gaoxin.mop;

import com.gaoxin.mop.config.HBaseConfig;
import com.gaoxin.mop.config.HBaseFactoryBean;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Mr.tan
 * Date:  2018/03/06 11:31
 * Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring.xml"})
@ComponentScan
public class BaseSpringTest {


    //java config形式注入
//    @PostConstruct
//    public HBaseFactoryBean initializeHBaseFactoryBean() throws Exception {
//        HBaseFactoryBean hBaseFactory = HBaseFactoryBean.getInstance();
//        List<HBaseConfig> list = new ArrayList<>();
//        list.add(new HBaseConfig("hmaster001.bj,rs001.bj,rs002.bj,rs003.bj,rs004.bj", "2181"));
//        hBaseFactory.setHbaseConfigs(list);
//        hBaseFactory.initializeConnections();
//
//        return hBaseFactory;
//    }


}
