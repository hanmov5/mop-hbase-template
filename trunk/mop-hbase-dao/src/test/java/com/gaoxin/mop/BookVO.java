package com.gaoxin.mop;

import java.util.Date;

/**
 * 书籍VO
 * Author: Mr.tan
 * Date:  2017/09/21
 */
public class BookVO {

    private String rowkey;
    private String bookId;
    private String bookName;
    private String bookAuthorName;
    private String bookPic;
    private String bookType; //一级分类名称
    private String bookTypeId; //一级分类ID
    private String bookStatus; //连载状态 0 连载 1完结
    private String auditStatus; //审核状态 0为审核 1审核通过 -1审核未通过
    private String isgrounding; //状态 -1删除 0未上架 1上架
    private String desc;  // 介绍
    private Date addDate; //添加的时间：暂时用于收藏时间|阅读时间
    private String lastRecordHtml; //上次收藏或阅读记录的地址。根据bookValue对象中的bookdetail计算得来，默认第一章
    private String lastRecordChapter;//上次收藏或阅读记录的章节rowKey;
    private String lastRecordTitle; //上次阅读章节标题
    private String bookWords;
    private String updateTime;
    private Integer userCounts;
    private BookValue bookValue;
    private String authordesc;    //作者简介
    private String bookHtml; //书籍HTML名称
    private String imgminiejs; //压缩图

    //当前章节-总章节序号
    private String totalChapters;
    private String currentChapter;
    //最新章节标题
    private String latestChapterTitle;
    private String latestChapterRow;
    private Long replynum;
    private String tag;
    //书籍是否阅读标识
    private boolean readFlag;

    public boolean isReadFlag() {
        return readFlag;
    }

    public void setReadFlag(boolean readFlag) {
        this.readFlag = readFlag;
    }

    public String getLastRecordTitle() {
        return lastRecordTitle;
    }

    public void setLastRecordTitle(String lastRecordTitle) {
        this.lastRecordTitle = lastRecordTitle;
    }

    public String getLatestChapterRow() {
        return latestChapterRow;
    }

    public void setLatestChapterRow(String latestChapterRow) {
        this.latestChapterRow = latestChapterRow;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getReplynum() {
        return replynum;
    }

    public void setReplynum(Long replynum) {
        this.replynum = replynum;
    }

    public String getImgminiejs() {
        return imgminiejs;
    }

    public void setImgminiejs(String imgminiejs) {
        this.imgminiejs = imgminiejs;
    }

    public String getBookHtml() {
        return bookHtml;
    }

    public void setBookHtml(String bookHtml) {
        this.bookHtml = bookHtml;
    }

    public String getBookTypeId() {
        return bookTypeId;
    }

    public void setBookTypeId(String bookTypeId) {
        this.bookTypeId = bookTypeId;
    }

    public String getLatestChapterTitle() {
        return latestChapterTitle;
    }

    public void setLatestChapterTitle(String latestChapterTitle) {
        this.latestChapterTitle = latestChapterTitle;
    }

    public String getTotalChapters() {
        return totalChapters;
    }

    public void setTotalChapters(String totalChapters) {
        this.totalChapters = totalChapters;
    }

    public String getCurrentChapter() {
        return currentChapter;
    }

    public void setCurrentChapter(String currentChapter) {
        this.currentChapter = currentChapter;
    }


    public String getRowkey() {
        return rowkey;
    }

    public void setRowkey(String rowkey) {
        this.rowkey = rowkey;
    }

    public Integer getUserCounts() {
        return userCounts;
    }

    public void setUserCounts(Integer userCounts) {
        this.userCounts = userCounts;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getBookWords() {
        return bookWords;
    }

    public void setBookWords(String bookWords) {
        this.bookWords = bookWords;
    }

    public String getIsgrounding() {
        return isgrounding;
    }

    public void setIsgrounding(String isgrounding) {
        this.isgrounding = isgrounding;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getLastRecordChapter() {
        return lastRecordChapter;
    }

    public void setLastRecordChapter(String lastRecordChapter) {
        this.lastRecordChapter = lastRecordChapter;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(String bookStatus) {
        this.bookStatus = bookStatus;
    }

    public String getLastRecordHtml() {
        return lastRecordHtml;
    }

    public void setLastRecordHtml(String lastRecordHtml) {
        this.lastRecordHtml = lastRecordHtml;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public BookValue getBookValue() {
        return bookValue;
    }

    public void setBookValue(BookValue bookValue) {
        this.bookValue = bookValue;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthorName() {
        return bookAuthorName;
    }

    public void setBookAuthorName(String bookAuthorName) {
        this.bookAuthorName = bookAuthorName;
    }

    public String getBookPic() {
        return bookPic;
    }

    public void setBookPic(String bookPic) {
        this.bookPic = bookPic;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public String getAuthordesc() {
        return authordesc;
    }

    public void setAuthordesc(String authordesc) {
        this.authordesc = authordesc;
    }
}
