package com.gaoxin.mop;


import com.gaoxin.mop.annotation.RowKey;


/**
 * 测试用 bean
 */
public class MopBookBean {
    @RowKey
    private String rowkey;// 	longmax-ctsrdts
    private String bookname;// 书名 去空格 回车 换行
    private String bookid;//	id （ctsrdts）


    public String getRowkey() {
        return rowkey;
    }

    public void setRowkey(String rowkey) {
        this.rowkey = rowkey;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    @Override
    public String toString() {
        return "MopBookBean{" +
                "rowkey='" + rowkey + '\'' +
                ", bookname='" + bookname + '\'' +
                ", bookid='" + bookid + '\'' +
                '}';
    }
}
